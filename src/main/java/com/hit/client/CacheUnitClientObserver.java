package com.hit.client;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.util.EventListener;
import java.util.List;

import javax.swing.JFileChooser;

import com.hit.view.CacheUnitView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class CacheUnitClientObserver implements PropertyChangeListener, EventListener{
	
	private CacheUnitClient cuc;
	
	public CacheUnitClientObserver() {
		this.cuc = new CacheUnitClient();
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if(!(evt.getSource() instanceof CacheUnitView))
			return;
		
		CacheUnitView cuv = (CacheUnitView)evt.getSource();
		
		switch(evt.getPropertyName()) {
			case "loadRequest":
				cuv.openFileChooser();
				break;
			case "sendRequest":
				JFileChooser chooser = (JFileChooser)evt.getNewValue();
				File selectedFile = chooser.getSelectedFile();
				onSelectedRequestFile(cuv, selectedFile);
				break;
			case "getStats":
				getStats(cuv);
				break;
		}
		
	}
	
	private void onSelectedRequestFile(CacheUnitView cuv, File selectedFile) {
		String json = sendRequest(selectedFile);
		
		if(json == null || json == "")
		{
			cuv.updateUIData(null);
			return;
		}
		
		Response<?> response = getResponseFromJson(json);
		if(response == null || !response.isSuccessed())
		{
			cuv.updateUIData(null);
			return;
		}
		
		switch(response.getType()) {
			case "GET":
				updateUIWithReceivedModels(cuv, json);
				break;
			case "UPDATE":
			case "DELETE":
				cuv.updateUIData(response);
				if(response.isSuccessed())
					getStats(cuv);
				break;
			case "GETSTATS":
				updateUIWithStats(cuv, json);
				break;
		}
	}
	
	private void updateUIWithStats(CacheUnitView cuv, String json) {
		Type ref = new TypeToken<Response<StatsModel>>(){}.getType(); 
		Response<StatsModel> statsModel = (new Gson()).fromJson(json, ref);;
		cuv.updateUIData(statsModel);
	}
	
	private Response<?> getResponseFromJson(String json){
		Type ref = new TypeToken<Response<?>>(){}.getType(); 
		Response<?> response = (new Gson()).fromJson(json, ref);
		return response;
	}
	
	private void updateUIWithReceivedModels(CacheUnitView cuv, String json) {
		Type ref = new TypeToken<Response<DataModel[]>>(){}.getType(); 
		Response<DataModel[]> response = (new Gson()).fromJson(json, ref);
		cuv.updateUIData(response);
		getStats(cuv);
	}
	
	private void getStats(CacheUnitView cuv) {
		File statsFile = new File("src/main/resources/GETSTATS.json");
		onSelectedRequestFile(cuv, statsFile);
	}
	
	private String sendRequest(File selectedFile) {
		try {
			String request = toString(Files.readAllLines(selectedFile.toPath()));
			String response = this.cuc.send(request);
			System.out.println(response);
			return response;
			
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String toString(List<String> lines) {
	    StringBuilder sb = new StringBuilder();
	    for (String line : lines) {
			sb.append(line);
		}
	    return sb.toString();
	}

}
