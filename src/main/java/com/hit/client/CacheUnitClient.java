package com.hit.client;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class CacheUnitClient {

	public CacheUnitClient() {

	}

	public String send(String request) {
		String response = "Failed";
		try (Socket clientSocket = new Socket("127.0.0.1", 12345)) {
			try (PrintWriter pw = new PrintWriter(clientSocket.getOutputStream())) {
				pw.println(request);
				pw.flush();
				try (Scanner scn = new Scanner(clientSocket.getInputStream())) {
					String serverData = scn.nextLine();
					response = serverData;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response;
	}
}
