package com.hit.client;


public class DataModel{

	private Long id;
	private String content;

	public DataModel(Long id, String content) {
		this.id = id;
		this.content = content;
	}
	
	public String getContent() {
		return this.content;
	}
	
	public Long getDataModelId(){
		return this.id;
	}
	

	public void setContent(String content) {
		this.content = content;
	}
	
	public void setDataModelId(Long id) {
		this.id = id;
	}
	
	
}
