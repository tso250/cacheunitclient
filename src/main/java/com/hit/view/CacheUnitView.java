package com.hit.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import com.hit.client.DataModel;
import com.hit.client.Response;
import com.hit.client.StatsModel;


public class CacheUnitView {

	private PropertyChangeSupport listeners;
	private JFrame frame;
	private CacheUnitPanel panel;
	JFileChooser fileChooser;
	public CacheUnitView() {
		this.listeners = new PropertyChangeSupport(this);
		this.frame = new JFrame("Cache Unit GUI");
		this.panel = new CacheUnitPanel();
		this.initFrameGraphics();
	}

	public void initFrameGraphics() {
		this.frame.setSize(1000, 500);
		this.frame.getContentPane().setLayout(null);
		this.frame.getContentPane().add(panel, BorderLayout.CENTER);
		this.panel.setSize(1000, 500);
		this.fileChooser = new JFileChooser();
		this.fileChooser.setSize(1000, 500);
		File dir = new File("src/main/resources");
		this.fileChooser.setCurrentDirectory(dir);
		this.fileChooser.addActionListener(this.panel);

	}

	public void start() {
		this.panel.setVisible(true);
		this.frame.setVisible(true);
		listeners.firePropertyChange("getStats", "getStats", "hello");
	}

	public void openFileChooser() {
		this.fileChooser.showOpenDialog(this.frame);
	}

	public void addPropertyChangeListener(PropertyChangeListener pcl) {
		this.listeners.addPropertyChangeListener(pcl);
	}

	public void removePropertyChangeListener(PropertyChangeListener pcl) {
		this.listeners.removePropertyChangeListener(pcl);
	}

	public <T> void updateUIData(T t) {
		Response<?> response = (Response<?>)t;
		
		if(response == null)
		{
			this.panel.setResponseText("Failed");
			return;
		}
		
		if(!response.isSuccessed())
		{
			this.panel.setResponseText("Failed");
			return;
		}
		
		switch(response.getType()) {
			case "GET":
				updateUIWithReceivedDataModels((DataModel[])response.getData());
			break;
			case "UPDATE":
			case "DELETE":
				this.panel.getContentTbl().setRowCount(0);
				this.panel.setResponseText("Successed");
			break;
			case "GETSTATS":
				this.panel.updateStats((StatsModel)response.getData());
			break;
			case "FAILED":
				this.panel.setResponseText("Failed");
				break;
			default:
				this.panel.setResponseText("Failed");
				break;
		}
		
	}
	
	private void updateUIWithReceivedDataModels(DataModel[] data) {
		this.panel.getContentTbl().setRowCount(0);
		
		if(data != null) {
			this.panel.setResponseText("Successed");
		}
		else {
			this.panel.setResponseText("Failed");
			return;
		}
		
		for(DataModel dm : data) {
			if(dm != null)
				this.panel.getContentTbl().addRow(new Object[] {dm.getDataModelId(), dm.getContent()});
		}
	}

	public class CacheUnitPanel extends JPanel implements ActionListener{

		private static final long serialVersionUID = 1L;
		private JTextField textField_1;
		private JTextField textField_capacity;
		private JTextField textField_algo;
		private JTextField textField_4;
		private JTextField textField_5;
		private JTextField textField_nOfRequests;
		private JTextField textField_nOfDataModels;
		private JTextField textField_8;
		private JTextField textField_9;
		private JTextField textField_nOfSwaps;
		private JPanel panel_1;
		private JTextField txtResponse;
		private JScrollPane scrollPane;
		private JTable table;
		private DefaultTableModel contentTbl;
		private JTextField textField_responseTxt;
		
		
		public CacheUnitPanel() {
			drawPanel();
			drawCommands();
			drawStatisticsComponents();
			drawDataModelsComponenets();
		}
		
		private void drawPanel() {
			this.setSize(1024, 800);
			this.setBackground(SystemColor.activeCaptionBorder);
			setLayout(null);

		}
		
		private void drawCommands() {
			
			JButton btnLoadReq = new JButton();
			btnLoadReq.setActionCommand("LoadRequest");
			btnLoadReq.addActionListener(this);
			btnLoadReq.setFont(new Font("Tahoma", Font.PLAIN, 30));
			btnLoadReq.setLocation(17, 19);
			btnLoadReq.setName("btnLoadReq");
			btnLoadReq.setText("Load Request");
			btnLoadReq.setSize(450, 51);
			btnLoadReq.setBackground(SystemColor.activeCaption);
			this.add(btnLoadReq);
			
			JButton btnStatistics = new JButton();
			btnStatistics.setActionCommand("GETSTATS");
			btnStatistics.addActionListener(this);
			btnStatistics.setFont(new Font("Tahoma", Font.PLAIN, 30));
			btnStatistics.setText("Statistics");
			btnStatistics.setName("btnLoadStats");
			btnStatistics.setBackground(SystemColor.activeCaption);
			btnStatistics.setBounds(484, 19, 463, 51);
			add(btnStatistics);
		}
		
		private void drawDataModelsComponenets() {
			
			panel_1 = new JPanel();
			panel_1.setBounds(17, 77, 450, 340);
			add(panel_1);
			panel_1.setLayout(null);
			panel_1.setBackground(Color.LIGHT_GRAY);
			
			txtResponse = new JTextField();
			txtResponse.setText("Response");
			txtResponse.setHorizontalAlignment(SwingConstants.CENTER);
			txtResponse.setFont(new Font("Tahoma", Font.PLAIN, 16));
			txtResponse.setEditable(false);
			txtResponse.setColumns(10);
			txtResponse.setBackground(Color.LIGHT_GRAY);
			txtResponse.setBounds(17, 19, 126, 26);
			panel_1.add(txtResponse);
			
			scrollPane = new JScrollPane();
			scrollPane.setBackground(Color.LIGHT_GRAY);
			scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPane.setBounds(17, 57, 416, 264);
			panel_1.add(scrollPane);
			
			contentTbl = new DefaultTableModel(new Object[] {"Id", "Content"}, 0);
			
			table = new JTable(contentTbl);
			table.getColumnModel().getColumn(0).setPreferredWidth(50);
			table.getColumnModel().getColumn(1).setPreferredWidth(380);
			table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
			table.setBackground(Color.LIGHT_GRAY);
			table.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
			table.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
			
			scrollPane.setViewportView(table);
			
			textField_responseTxt = new JTextField();
			textField_responseTxt.setEditable(false);
			textField_responseTxt.setHorizontalAlignment(SwingConstants.CENTER);
			textField_responseTxt.setFont(new Font("Tahoma", Font.PLAIN, 14));
			textField_responseTxt.setColumns(10);
			textField_responseTxt.setBackground(Color.LIGHT_GRAY);
			textField_responseTxt.setBounds(146, 19, 287, 26);
			panel_1.add(textField_responseTxt);
		}
		
		private void drawStatisticsComponents() {
			JPanel panel = new JPanel();
			panel.setBackground(Color.LIGHT_GRAY);
			panel.setBounds(484, 77, 463, 340);
			add(panel);
			panel.setLayout(null);
			
			textField_1 = new JTextField();
			textField_1.setHorizontalAlignment(SwingConstants.LEFT);
			textField_1.setText("Capacity");
			textField_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
			textField_1.setEditable(false);
			textField_1.setColumns(10);
			textField_1.setBackground(Color.LIGHT_GRAY);
			textField_1.setBounds(38, 33, 240, 30);
			panel.add(textField_1);
			
			textField_capacity = new JTextField();
			textField_capacity.setHorizontalAlignment(SwingConstants.CENTER);
			textField_capacity.setText("0");
			textField_capacity.setFont(new Font("Tahoma", Font.PLAIN, 15));
			textField_capacity.setEditable(false);
			textField_capacity.setColumns(10);
			textField_capacity.setBackground(Color.LIGHT_GRAY);
			textField_capacity.setBounds(295, 33, 118, 30);
			panel.add(textField_capacity);
			
			textField_algo = new JTextField();
			textField_algo.setHorizontalAlignment(SwingConstants.CENTER);
			textField_algo.setText("None");
			textField_algo.setFont(new Font("Tahoma", Font.PLAIN, 15));
			textField_algo.setEditable(false);
			textField_algo.setColumns(10);
			textField_algo.setBackground(Color.LIGHT_GRAY);
			textField_algo.setBounds(295, 82, 118, 30);
			panel.add(textField_algo);
			
			textField_4 = new JTextField();
			textField_4.setHorizontalAlignment(SwingConstants.LEFT);
			textField_4.setText("Algorithm");
			textField_4.setFont(new Font("Tahoma", Font.PLAIN, 15));
			textField_4.setEditable(false);
			textField_4.setColumns(10);
			textField_4.setBackground(Color.LIGHT_GRAY);
			textField_4.setBounds(38, 83, 240, 30);
			panel.add(textField_4);
			
			textField_5 = new JTextField();
			textField_5.setHorizontalAlignment(SwingConstants.LEFT);
			textField_5.setText("Total Number Of Requests");
			textField_5.setFont(new Font("Tahoma", Font.PLAIN, 15));
			textField_5.setEditable(false);
			textField_5.setColumns(10);
			textField_5.setBackground(Color.LIGHT_GRAY);
			textField_5.setBounds(38, 132, 240, 30);
			panel.add(textField_5);
			
			textField_nOfRequests = new JTextField();
			textField_nOfRequests.setHorizontalAlignment(SwingConstants.CENTER);
			textField_nOfRequests.setText("0");
			textField_nOfRequests.setFont(new Font("Tahoma", Font.PLAIN, 15));
			textField_nOfRequests.setEditable(false);
			textField_nOfRequests.setColumns(10);
			textField_nOfRequests.setBackground(Color.LIGHT_GRAY);
			textField_nOfRequests.setBounds(295, 132, 118, 30);
			panel.add(textField_nOfRequests);
			
			textField_nOfDataModels = new JTextField();
			textField_nOfDataModels.setHorizontalAlignment(SwingConstants.CENTER);
			textField_nOfDataModels.setText("0");
			textField_nOfDataModels.setFont(new Font("Tahoma", Font.PLAIN, 15));
			textField_nOfDataModels.setEditable(false);
			textField_nOfDataModels.setColumns(10);
			textField_nOfDataModels.setBackground(Color.LIGHT_GRAY);
			textField_nOfDataModels.setBounds(295, 182, 118, 30);
			panel.add(textField_nOfDataModels);
			
			textField_8 = new JTextField();
			textField_8.setHorizontalAlignment(SwingConstants.LEFT);
			textField_8.setText("Total Number Of Data Models Sent");
			textField_8.setFont(new Font("Tahoma", Font.PLAIN, 15));
			textField_8.setEditable(false);
			textField_8.setColumns(10);
			textField_8.setBackground(Color.LIGHT_GRAY);
			textField_8.setBounds(38, 182, 240, 30);
			panel.add(textField_8);
			
			textField_9 = new JTextField();
			textField_9.setHorizontalAlignment(SwingConstants.LEFT);
			textField_9.setText("Total Number Of Swaps");
			textField_9.setFont(new Font("Tahoma", Font.PLAIN, 15));
			textField_9.setEditable(false);
			textField_9.setColumns(10);
			textField_9.setBackground(Color.LIGHT_GRAY);
			textField_9.setBounds(38, 231, 240, 30);
			panel.add(textField_9);
			
			textField_nOfSwaps = new JTextField();
			textField_nOfSwaps.setHorizontalAlignment(SwingConstants.CENTER);
			textField_nOfSwaps.setText("0");
			textField_nOfSwaps.setFont(new Font("Tahoma", Font.PLAIN, 15));
			textField_nOfSwaps.setEditable(false);
			textField_nOfSwaps.setColumns(10);
			textField_nOfSwaps.setBackground(Color.LIGHT_GRAY);
			textField_nOfSwaps.setBounds(295, 231, 118, 30);
			panel.add(textField_nOfSwaps);
		}
		
		public DefaultTableModel getContentTbl() {
			return contentTbl;
		}
		
		public void updateStats(StatsModel sm) {
			if(sm == null) {
				this.textField_responseTxt.setText("Failed");
				return;
			}
				
			this.textField_algo.setText(sm.getAlgoName());
			this.textField_capacity.setText(sm.getCapacity().toString());
			this.textField_nOfDataModels.setText(sm.getTotalDataModels().toString());
			this.textField_nOfRequests.setText(sm.getTotalRequests().toString());
			this.textField_nOfSwaps.setText(sm.getTotalDataModelsSwaps().toString());
		}
		public void setResponseText(String data) {
			this.textField_responseTxt.setText(data);
		}

		@Override
		public void actionPerformed(ActionEvent action) {
			switch(action.getActionCommand()) {
				case "GETSTATS":
					listeners.firePropertyChange("getStats", "getStats", action.getSource());
					break;
				case "LoadRequest":
					listeners.firePropertyChange("loadRequest", "loadRequest", action.getSource());
					break;
				case "ApproveSelection":
					listeners.firePropertyChange("sendRequest", null, action.getSource());
					break;
					
			}
			
		}
	}
	
}
